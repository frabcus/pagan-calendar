#! /usr/bin/env python3

import icalendar
import pymeeus.Sun
import datetime
import pytz

cal = icalendar.Calendar()

# Timezone is needed as iCal files are timezone dependent - e.g. the longest
# day may be a different calendar date for different timezones for the same year.
tz = pytz.timezone('Europe/London')
#tz = pytz.timezone('Pacific/Honolulu')
#tz = pytz.timezone('Asia/Tokyo')

# Convert the exact time to output timezone, and then work out which date it
# falls on
def date_in_tz(when):
    when_date = when.astimezone(tz).date()
    return when_date

# Generates a calendar event and adds to the global iCal we're making
def make_cal_event(cal, summary, description, when_date):
    event = icalendar.Event()
    event.add('summary', summary)
    event.add('description', description)
    event.add('dtstart', when_date)
    event.add('dtend', when_date)
    cal.add_component(event)

# Return the equinox or solstice exact time for the given season
# i.e. When the tilt of the earth exactly points towards, away, or at right angles to the sun
def get_sun(year, season):
    when = pymeeus.Sun.Sun.get_equinox_solstice(year, target=season).get_full_date(local=True)
    when_datetime = datetime.datetime(when[0], when[1], when[2], when[3], when[4], int(when[5]))
    return date_in_tz(when_datetime)

for year in range(2000, 2100):
    # Cross-quarter days from "in Ireland" part of https://en.wikipedia.org/wiki/Quarter_days

    make_cal_event(cal, "Imbolc", "Beginning of spring, halfway between the winter solstice and the spring equinox: https://en.wikipedia.org/wiki/Imbolc", datetime.date(year, 2, 1))
    make_cal_event(cal, "Spring equinox", "Equal length day and night: https://en.wikipedia.org/wiki/Imbolc", get_sun(year, "spring"))

    make_cal_event(cal, "Beltane", "Beginning of summer, halfway between the spring equinox and summer solstice: https://en.wikipedia.org/wiki/Beltane", datetime.date(year, 5, 1))
    make_cal_event(cal, "Summer solstice", "Longest day of the year, also called midsummer: https://en.wikipedia.org/wiki/Summer_solstice", get_sun(year, "summer"))

    make_cal_event(cal, "Lughnasadh", "Beginning of the harvest season, halfway between the summer solstice and autumn equinox: https://en.wikipedia.org/wiki/Lughnasadh", datetime.date(year, 8, 1))
    make_cal_event(cal, "Autumn equinox", "Equal length day and night: https://en.wikipedia.org/wiki/September_equinox", get_sun(year, "autumn"))

    make_cal_event(cal, "Samhain", "Beginning of winter, halfway between the autumn equinox and winter solstice: https://en.wikipedia.org/wiki/Samhain", datetime.date(year, 11, 1))
    make_cal_event(cal, "Winter solstice", "Shortest day of the year: https://en.wikipedia.org/wiki/Winter_solstice", get_sun(year, "winter"))

f = open('pagan.ics', 'wb')
f.write(cal.to_ical())
f.close()
